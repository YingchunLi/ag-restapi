package com.yingchunli.fullstacktest.api.task;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskServiceImplTest {
    TaskService taskService = new TaskServiceImpl();

    @Test
    public void validateBrackets_returns_true_for_balanced_brackets() {
        assertTrue(taskService.isBracketsBalanced("({[abc]})"));
    }

    @Test
    public void validateBrackets_returns_true_when_no_brackets() {
        assertTrue(taskService.isBracketsBalanced("a"));
    }

    @Test
    public void validateBrackets_returns_false_for_missing_open_brackets() {
        assertFalse(taskService.isBracketsBalanced("bla)bla"));
    }

    @Test
    public void validateBrackets_returns_false_for_missing_close_brackets() {
        assertFalse(taskService.isBracketsBalanced("bla{bla"));
    }

    @Test
    public void validateBrackets_returns_false_when_close_bracket_comes_before_open_bracket() {
        assertFalse(taskService.isBracketsBalanced("]{}["));
    }

    @Test
    public void validateBrackets_returns_false_for_unmatched_brackets() {
        assertFalse(taskService.isBracketsBalanced("{[}]"));
    }
}
