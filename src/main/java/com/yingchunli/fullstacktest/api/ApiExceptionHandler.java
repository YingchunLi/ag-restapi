package com.yingchunli.fullstacktest.api;

import com.yingchunli.fullstacktest.api.ValidationErrorResponse.Detail;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {

        String name = "ValidationError";
        List<Detail> details = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            String location = "params";
            Path propertyPath = violation.getPropertyPath();
            final String[] param = {"unknown"};
            StreamSupport.stream(propertyPath.spliterator(), false)
                    .reduce((node, node2) -> node2)
                    .ifPresent(node -> param[0] = node.getName());
            String msg = violation.getMessage();
            String value = violation.getInvalidValue().toString();

            Detail detail = new Detail(location, param[0], msg, value);
            details.add(detail);
        }

        ValidationErrorResponse validationErrorResponse =
                new ValidationErrorResponse(name, details);
        return new ResponseEntity<>(
                validationErrorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        String name = "ValidationError";
        List<Detail> details =
                ex.getBindingResult().getFieldErrors().stream()
                        .map(fieldError -> new Detail("params", fieldError.getField(),
                                fieldError.getDefaultMessage(), fieldError.getRejectedValue().toString()))
                        .collect(Collectors.toList());
        ValidationErrorResponse validationErrorResponse =
                new ValidationErrorResponse(name, details);
        return new ResponseEntity<>(
                validationErrorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ TaskNotFoundException.class })
    public ResponseEntity<Object> handleTaskNotFound(
            TaskNotFoundException ex, WebRequest request) {

        String name = "NotFoundError";
        List<NotFoundExceptionResponse.Detail> details = new ArrayList<>();
        NotFoundExceptionResponse.Detail detail = new NotFoundExceptionResponse.Detail(ex.getMessage());
            details.add(detail);

        NotFoundExceptionResponse notFoundExceptionResponse =
                new NotFoundExceptionResponse(name, details);
        return new ResponseEntity<>(
                notFoundExceptionResponse, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
