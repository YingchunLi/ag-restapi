package com.yingchunli.fullstacktest.api.todo;

import javax.validation.constraints.Size;

public class PatchTodoRequest {
    @Size(min = 1, max = 50, message = "Must be between 1 and 50 chars long")
    private String text;

    private boolean isCompleted;

    public String getText() {
        return text;
    }

    public boolean getIsCompleted() {
        return isCompleted;
    }
}
