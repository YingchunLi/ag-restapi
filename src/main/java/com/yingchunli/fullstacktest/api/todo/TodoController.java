package com.yingchunli.fullstacktest.api.todo;

import com.yingchunli.fullstacktest.api.TaskNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/todo")
public class TodoController {
    @Autowired
    private TodoService todoService;

    @PostMapping
    public Todo createTodo(@Valid @RequestBody NewTodoRequest newTodo) {
        return todoService.createTodo(newTodo.getText());
    }

    @RequestMapping("/{id}")
    public Todo getTodo(@PathVariable(value="id") long id) throws TaskNotFoundException {
        return todoService.getTodo(id);
    }

    @PatchMapping("/{id}")
    public Todo updateTodo(@PathVariable(value="id") long id,
                           @Valid @RequestBody PatchTodoRequest newTodo) throws TaskNotFoundException {
        return todoService.updateTodo(id, newTodo.getText(), newTodo.getIsCompleted());
    }

}
