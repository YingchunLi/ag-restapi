package com.yingchunli.fullstacktest.api.todo;

import com.yingchunli.fullstacktest.api.TaskNotFoundException;

public interface TodoService {
    Todo createTodo(String text);
    Todo getTodo(long id) throws TaskNotFoundException;
    Todo updateTodo(long id, String text, boolean isCompleted) throws TaskNotFoundException;
}
