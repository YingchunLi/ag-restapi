package com.yingchunli.fullstacktest.api.todo;

import com.yingchunli.fullstacktest.api.TaskNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository todoRepository;

    @Override
    public Todo createTodo(String text) {
        Todo todo = new Todo(text);
        return todoRepository.save(todo);
    }

    @Override
    public Todo getTodo(long id) throws TaskNotFoundException {
        return todoRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException("Item with " + id + " not found"));
    }

    @Override
    public Todo updateTodo(long id, String text, boolean isCompleted) throws TaskNotFoundException {
        Todo todo = getTodo(id);
        if (text != null) {
            todo.setText(text);
        }

        todo.setIsCompleted(isCompleted);

        return todoRepository.save(todo);
    }
}
