package com.yingchunli.fullstacktest.api.todo;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Date;

@Entity
public class Todo {
    @Id
    @GeneratedValue
    private long id;

    @Size(min = 1, max = 50, message = "Must be between 1 and 50 chars long")
    private String text;

    private boolean isCompleted;
    private Instant createdAt;

    public Todo() {
    }

    public Todo(@Size(min = 1, max = 50, message = "Must be between 1 and 50 chars long") String text) {
        this.text = text;
        this.isCompleted = false;
        this.createdAt = Instant.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(boolean completed) {
        isCompleted = completed;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }
}
