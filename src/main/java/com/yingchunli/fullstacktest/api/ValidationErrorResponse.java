package com.yingchunli.fullstacktest.api;

import java.util.List;

public class ValidationErrorResponse {

    static class Detail {
        private String location;
        private String param;
        private String msg;
        private String value;

        public Detail(String location, String param, String msg, String value) {
            this.location = location;
            this.param = param;
            this.msg = msg;
            this.value = value;
        }

        public String getLocation() {
            return location;
        }

        public String getParam() {
            return param;
        }

        public String getMsg() {
            return msg;
        }

        public String getValue() {
            return value;
        }
    }

    private List<Detail> details;
    private String name;

    public ValidationErrorResponse(String name, List<Detail> details) {
        this.details = details;
        this.name = name;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public String getName() {
        return name;
    }
}
