package com.yingchunli.fullstacktest.api;

import java.util.List;

public class NotFoundExceptionResponse {

    static class Detail {
        private String message;

        public Detail(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    private List<Detail> details;
    private String name;

    public NotFoundExceptionResponse(String name, List<Detail> details) {
        this.details = details;
        this.name = name;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public String getName() {
        return name;
    }
}
