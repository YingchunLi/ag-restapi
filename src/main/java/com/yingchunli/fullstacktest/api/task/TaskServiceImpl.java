package com.yingchunli.fullstacktest.api.task;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

@Service
public class TaskServiceImpl implements TaskService {
    public static final char LEFT_PARENTHESES = '(';
    public static final char RIGHT_PARENTHESES = ')';
    public static final char LEFT_SQUARE_BRACKET = '[';
    public static final char RIGHT_SQUARE_BRACKET = ']';
    public static final char LEFT_BRACE = '{';
    public static final char RIGHT_BRACE = '}';

    private static Map<Character, Character> MATCHING_BRACKETS = new HashMap<>();

    static {
        MATCHING_BRACKETS.put(RIGHT_PARENTHESES, LEFT_PARENTHESES);
        MATCHING_BRACKETS.put(RIGHT_SQUARE_BRACKET, LEFT_SQUARE_BRACKET);
        MATCHING_BRACKETS.put(RIGHT_BRACE, LEFT_BRACE);
    }

    @Override
    public boolean isBracketsBalanced(String input) {
        Stack<Character> openBrackets = new Stack<>();

        for (int index = 0; index < input.length(); index++) {
            char c = input.charAt(index);
            switch (c) {
                case LEFT_PARENTHESES:
                case LEFT_SQUARE_BRACKET:
                case LEFT_BRACE:
                    openBrackets.push(c);
                    break;

                case RIGHT_PARENTHESES:
                case RIGHT_SQUARE_BRACKET:
                case RIGHT_BRACE:
                    if (openBrackets.empty() || openBrackets.pop() != MATCHING_BRACKETS.get(c)) {
                        return false;
                    }
                    break;
            }
        }

        return openBrackets.empty();
    }
}
