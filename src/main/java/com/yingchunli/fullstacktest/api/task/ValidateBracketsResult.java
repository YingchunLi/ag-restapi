package com.yingchunli.fullstacktest.api.task;

public class ValidateBracketsResult {
    private String input;
    private boolean isBalanced;

    public ValidateBracketsResult(String input, boolean isBalanced) {
        this.input = input;
        this.isBalanced = isBalanced;
    }

    public String getInput() {
        return input;
    }

    public boolean getIsBalanced() {
        return isBalanced;
    }

}
