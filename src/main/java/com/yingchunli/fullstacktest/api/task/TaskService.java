package com.yingchunli.fullstacktest.api.task;

public interface TaskService {
    boolean isBracketsBalanced(String input);
}
