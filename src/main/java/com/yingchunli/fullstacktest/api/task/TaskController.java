package com.yingchunli.fullstacktest.api.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;

@RestController
@Validated
public class TaskController {
    @Autowired
    private TaskService taskService;

    @RequestMapping("/tasks/validateBrackets")
    public ValidateBracketsResult validateBrackets(@Size(min=1, max = 50, message = "Must be between 1 and 50 chars long")
                                           @RequestParam(name = "input") String text) {
        boolean isBalanced = taskService.isBracketsBalanced(text);
        ValidateBracketsResult result = new ValidateBracketsResult(text, isBalanced);
        return result;
    }
}
